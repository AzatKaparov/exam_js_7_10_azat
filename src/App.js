import React from "react";
import "./App.css";
import Cafe from "./containers/Cafe";


function App() {
  return (
    <div className="container">
      <Cafe/>
    </div>
  );
}

export default App;
