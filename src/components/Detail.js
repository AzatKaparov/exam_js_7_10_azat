import React from 'react';

const Detail = ({ name, count, price, onRemove }) => {
    return (
        <div className="detail-item">
            <p className="detail-name">{name}</p>
            <p className="detail-count">x{count}</p>
            <p className="detail-price">{price * count}</p>
            <button onClick={onRemove} className="detail-remove">Delete</button>
        </div>
    );
};

export default Detail;