import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import Detail from "./Detail";

const Details = ({ initialData, stateData, removeDish, totalPrice }) => {
    return (
        <div className="details-block">
            <h3>Order details</h3>
            {initialData.map((item, idx) => {
                if (stateData[idx].count !== 0) {
                    return (
                        <Detail
                            key={uuidv4()}
                            name={item.name}
                            price={item.price}
                            count={stateData[idx].count}
                            onRemove={() => removeDish(item.name)}
                        />
                    )
                }
                return null;
            })}
            <h5>Total price: {totalPrice}</h5>
        </div>
    );
};

export default Details;