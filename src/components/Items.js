import React from 'react';
import Item from "./Item";
import { v4 as uuidv4 } from 'uuid';

const Items = ({ initialData, addDish }) => {
    return (
        <div className="items-block">
            <h3>Add items</h3>
            <div className="items">
                {initialData.map(item => {
                    return (
                        <Item
                            key={uuidv4()}
                            onDishClick={() => addDish(item.name)}
                            name={item.name}
                            price={item.price}
                        />
                    )
                })}
            </div>
        </div>
    );
};

export default Items;