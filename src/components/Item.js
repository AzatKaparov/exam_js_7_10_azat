import React from 'react';

const Item = ({ name, price, onDishClick }) => {
    return (
        <div className="item" onClick={onDishClick}>
            <p className="item-name">{name}
                <span className="item-price">Price: {price}</span>
            </p>
        </div>
    );
};

export default Item;