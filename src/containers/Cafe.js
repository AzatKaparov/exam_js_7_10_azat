import React, {useState} from 'react';
import Items from "../components/Items";
import Details from "../components/Details";

const DISHES = [
    {name: "Hamburger", price: 80},
    {name: "Cheeseburger", price: 90},
    {name: "Fries", price: 45},
    {name: "Coffee", price: 70},
    {name: "Tea", price: 50},
    {name: "Cola", price: 40}
];

const Cafe = () => {
    const [dishes, setDishes] = useState([
        {name: "Hamburger", count: 0},
        {name: "Cheeseburger", count: 0},
        {name: "Fries", count: 0},
        {name: "Coffee", count: 0},
        {name: "Tea", count: 0},
        {name: "Cola", count: 0},
    ]);

    const [totalPrice, setTotalPrice] = useState(0);

    const getTotalPrice = massive => {
        let total = 0;
        massive.forEach((item, idx) => {
            let counter = 0;
            while (counter !== item.count) {
                total += DISHES[idx].price;
                counter++;
            }
        });
        return total;
    };

    const addDish = name => {
        const newDishes = dishes.map(item => {
            if (item.name === name) {
                return {
                    ...item,
                    count: item.count += 1
                };
            }
            return item;
        });
        setTotalPrice(getTotalPrice(newDishes));
        setDishes(newDishes);
    };

    const removeDish = name => {
        const newDishes = dishes.map(item => {
            if (item.name === name) {
                return {
                    ...item,
                    count: 0
                };
            }
            return item;
        });
        setTotalPrice(getTotalPrice(newDishes));
        setDishes(newDishes);
    }

    return (
        <div className="main-content">
            <Items
                addDish={addDish}
                initialData={DISHES}
            />
            <Details
                initialData={DISHES}
                stateData={dishes}
                removeDish={removeDish}
                totalPrice={totalPrice}
            />
        </div>
    );
};

export default Cafe;